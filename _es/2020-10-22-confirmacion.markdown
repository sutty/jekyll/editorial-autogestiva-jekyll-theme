---
draft: false
categories: []
layout: confirmation
title: "¡Gracias por tu compra!"
permalink: confirmacion/
back: Volver al sitio
tags: []
---

En breve te llegará un correo con la información de tu pedido.
