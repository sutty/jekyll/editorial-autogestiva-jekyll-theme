---
draft: false
categories: []
layout: cart
title: Carrito de compras
product: Libro
currency: ARS
currency_alternate: ARS
price: Precio
quantity: Cantidad
subtotal: Subtotal
total: Total
add: Agregar al carrito
added: Agregado al carrito
out_of_stock: Agotado
remove: Quitar del carrito
back: Volver a la tienda
next_step: Comprar
permalink: carrito/
tags: []
---

