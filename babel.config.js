module.exports = api => {
  api.cache(true)

  return {
    plugins: ['@babel/plugin-proposal-class-properties'],
    presets: [
      [
        "@babel/preset-env",
        {
          useBuiltIns: "entry",
          targets: "> 0.25%, not dead",
          shippedProposals: true,
          corejs: { version: "3.9" }
        }
      ]
    ]
  }
}
